---
layout: page
title: À Propos
---

Don't Leave the Road est un jeu de rôle à vocation horrifique, né de l'imagination d'Antoine Lenoir en 2018.

Le système de jeu se veut simple, rapide et létal, et s'axe autour d'un des dés les plus mal-aimés du jeu de rôle - le __dodécaèdre__, aussi appelé __Dédouze__ par les connaisseurs.

Vous trouverez ici les règles du jeu, disponibles sous licence Creative Commons, ainsi que des liens de téléchargement pour le contenu en PDF.
