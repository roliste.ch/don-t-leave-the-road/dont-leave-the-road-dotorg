---
layout: page
title: 04 - Boîte à outils du MJ
toc: true
---

## Gérer la difficulté d'une action

Tout ou presque est hostile dans ce monde en perdition. Lorsqu'un personnage est confronté à une adversité, voici quelques pistes pour fixer la difficulté d'une action

<table class="table"><thead><tr><th>Difficulté de l'action</th><th>Seuil de réussite</th></tr></thead><tbody><tr><td>Très facile<br></td><td>6<br></td></tr><tr><td>Facile<br></td><td>9<br></td></tr><tr><td>Moyen<br></td><td>12<br></td></tr><tr><td>Difficile<br></td><td>15<br></td></tr><tr><td>Très difficile<br></td><td>18<br></td></tr></tbody></table>

On peut également ajouter des modificateurs aux actions des personnages, selon le contexte de l'action.

## Adversaires et créatures

Une créature ou un adversaire peut être définie de plusieurs manières différentes, selon qu'il s'agit d'un adversaire normal ou d'un adversaire principal.

* On considère comme **normal** un adversaire dont les PJ peuvent venir à bout *relativement* facilement. (Tout est relatif, bien sûr)
* On condidère comme **principal** un adversaire plus dangereux (!), et qui nécessitera beaucoup d'efforts pour être défait (si toutefois les PJ décident de l'affronter, ce qui est rarement une bonne idée)

### Adversaires normaux

Les adversaires dits normaux sont uniquement définies par un score d'adversité (correspondant à une seule et unique caractéristique, avec sa valeur et sa réserve)

Lors de la confrontation, on affecte les dégâts sur la réserve de cette unique caractéristique, et l'adversaire est considéré comme vaincu lorsque la réserve atteint 0.

Ces adversaires peuvent également posséder des attributs de créatures ou des traits (pour un PNJ).

### Adversaires principaux

Les adversaires principaux sont définis comme un PJ, à savoir par les trois caractéristiques de Corps, Esprit et Âme.

Les adversaires principaux possèdent au minimum un attribut de créature (ou un trait pour un PNJ). Leur valeur de caractéristiques peut, si l'adversaire a l'attribut "Caractéristique exceptionnelle", atteindre 15.

### Exemples d'attributs de créatures

Voici une liste non-exhaustive d'attributs pour les créatures. D'autres attributs sont disponibles dans les bestiaires des différents univers.

<table>
<thead>
<tr>
  <th>Attribut de créature</th>
  <th>Description de l'attribut</th>
</tr>
</thead>
<tbody>
<tr>
  <td>Aberrante</td>
  <td>La forme de la créature est complètement aberrante. La créature est immunisée aux coups critiques.</td>
</tr>
<tr>
  <td>Ailes</td>
  <td>La créature peut voler.</td>
</tr>
<tr>
  <td>Blessure effroyable <em>(Score)</em></td>
  <td>La créature est capable d'infliger des blessures épouvantables. Chaque tour, elle peut choisir d'utiliser une seule et unique attaque. Si l'attaque touche, elle inflige <em>[Score de blessure effroyable]</em> points de dégâts sur la <strong>Valeur de caractéristique</strong> au lieu de la réserve.</td>
</tr>
<tr>
  <td>Caractéristique exceptionnelle <em>(Carac)</em></td>
  <td>La caractéristique en question peut dépasser 12.</td>
</tr>
<tr>
  <td>Nuée</td>
  <td>L'adversaire est composé d'une multitude de petites créatures. Les dégâts reçus sont limités à 1 (2 en cas de réussite critique).</td>
</tr>
<tr>
  <td>Peur <em>(Score)</em></td>
  <td>La créature est effrayante. Les personnages en présence de cette créature doivent faire un test d'Esprit à difficulté 12 sous peine de perdre <em>[Score de peur]</em> points de leur réserve d'Esprit.</td>
</tr>
</tbody>
</table>

### Exemples d'adversaires

Ces exemples sont tirés du bestiaire d'**Unterwald**.

#### Pillard famélique

Un homme au teint buriné par les éléments et aux traits émaciés. Il porte une armure de cuir en lambeaux et tient en main un couteau de boucher fort inquiétant.

**Adversité:** 6

**Attributs:** *aucun*

**Traits:** Eclaireur (1), Larron (1)

**Protection:** Armure de cuir (1)

**Armes:** Couteau de boucher (1)

#### Chiroptère

Les chiroptères sont des monstruosités ailées, de quatre mètres d’envergure, aux griffes acérées et aux crocs suintant un ichor noirâtre empoisonné.

Ils attaquent en groupe et tentent d’emporter leurs proies dans les airs en les agrippant avec leurs serres. Si c’est le cas, ils montent ensuite à une altitude suffisante avant de laisser la victime retomber dans les rochers.

Dans les autres cas, les chiroptères attaquent avec leurs griffes, leurs serres et leurs crocs (2 attaques par round, au libre choix du MJ)

**Adversité:** 8

**Attributs:** Ailes, Peur (2)

**Traits:** *aucun*

**Protection:** *aucune*

**Armes:** Griffes (1), Serres (2), Crocs (1 + Poison)

**Aptitudes spéciales:**

* *Plongeon mortel:* Une fois par round, le chiroptère peut tenter d'agripper un adversaire. Dans ce cas, il effectue une seule et unique attaque contre la défense de son adversaire. Si il réussit, l'adversaire est considéré comme agrippé. Il faut effectuer un test de **Corps + un Trait lié** à difficulté **13** pour se libérer, et potentiellement faire une chute qui n'est pas mortelle. Le chiroptère lâche sa cible dans les rochers après deux tours. Dans ce cas, son adversaire est tué.
* *Poison:* Sur une attaque de **Crocs** infligeant des dégâts, la cible doit réussir un test de **Corps** à difficulté **12** sous peine d'être empoisonnée. Elle encaisse à chaque tour un point de dommages de **Corps** jusqu'à ce qu'elle réussisse son test.
