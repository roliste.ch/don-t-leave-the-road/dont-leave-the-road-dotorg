---
layout: page
title: 02 - Combat et récupération
toc: true
---

## Le combat

Il arrive que les personnages soient obligés à combattre pour sauver leur misérable existence. Dans cette section, on abordera les règles des combats, des dommages et du soin.

### Défense passive ou active

La défense correspond au seuil de difficulté pour blesser un personnage (sur n'importe quelle des trois caractéristiques). Elle peut être soit active, soit passive.

Elle est dite **active** lorsque le personnage utilise son action du tour pour se défendre uniquement, et **passive** lorsqu'il cherche à agir lors de son tour.

La défense passive est égale à la **réserve de la caractéristique attaquée + 5**. Un personnage ayant une caractéristique de 5 en corps aura donc un seuil de difficulté de 10 pour être blessé par une attaque physique.

La défense active est définie par un jet de la caractéristique attaquée, soit **la réserve de caractéristique + un éventuel trait + 1d12**. En effet, même en se défendant de manière active, il est toujours possible de se tromper et d'ouvrir sa garde. Cela dit, un personnage formé au combat se défendra plus efficacement de manière active.

Dans le cas où une défense active est saluée par une **réussite critique**, on considèrera qu'une **réussite critique** de l'attaquant est nécessaire pour toucher. Dans le cas contraire, où le défenseur échoue de manière **critique**, on considèrera que les attaques le visant touchent automatiquement. L'attaquant doit néanmoins lancer le dé pour déterminer les dégâts qui seront infligés.

### Tours de combat

Un combat est défini en tours. Un tour se déroule de la manière suivante :

Les personnages-joueurs ont chacun deux actions, et agissent l'un après l'autre (ici, pas de règle d'initiative, laissez les joueurs s'arranger entre eux). Ils peuvent :

* Attaquer
* Se défendre activement
* Se déplacer
* Aider un allié
* Fuir (quitter la confrontation)

### Actions lors du combat

#### Attaquer

Une action d'attaque est effectuée par un jet de la caractéristique associée, à savoir le Corps pour les attaques physiques (au corps à corps ou à distance), l'Esprit pour les attaques mentales, et l'Âme pour les attaques sociales, contre la défense associée (active ou passive)

Si le jet est réussi, alors l'attaque inflige les **dégâts de l'arme + la marge de réussite**.

*TODO: Ajouter un exemple d'attaque*

#### Se défendre activement

Comme indiqué dans la section sur la défense, lorsqu'un personnage se défend, il utilise sa défense active au lieu de sa défense passive pour définir le seuil de difficulté.

#### Se déplacer

Dans ce jeu, point de battlemap quadrillée ou à hexagones. On considèrera uniquement des facteurs de distance, qui sont les suivants:

- **Contact** : Le personnage est au corps-à-corps avec son adversaire
- **Courte** : La portée courte correspond à *quelques mètres*
- **Moyenne** : Une portée moyenne correspond environ à *une vingtaine de mètres*
- **Longue** : La portée longue correspond à tout ce qui dépasse *cinquante mètres*

On peut utiliser une action de déplacement pour passer d'une portée *contact* à *courte* et inversement, ou de *courte* à *moyenne* et inversement.

Pour passer de la portée *moyenne* à *longue*, il est nécessaire d'utiliser **deux** actions de déplacement.

#### Aider un allié

Un personnage peut choisir d'aider un allié, soit à attaquer, soit à se défendre, soit à fuir un combat. Ce faisant, il doit effectuer un jet de **la caractéristique associée + un éventuel trait** contre une difficulté de **12**. En cas de réussite, son allié aura un bonus de **+3** à son jet.

#### Fuir une confrontation

Par moments, il est vraiment difficile d'affronter certaines créatures ou personnes. Aussi, il est possible de fuir une confrontation. Pour ce faire, il faut faire un jet de Corps (dans le cas d'un combat), d'Esprit (dans le cas d'une confrontation mentale) ou d'Âme (dans le cas d'une confrontation sociale). La difficulté de ce jet est de **12**.

* Si le jet est **réussi**, alors le personnage s'est échappé de la confrontation et ne pourra plus y participer à moins qu'il ne décide d'y reprendre part. Cela lui évite ainsi de souffrir de dommages lors de la confrontation.
* Si le jet est **une réussite avec complication**, le personnage s'est échappé de la confrontation, mais a subi un point de dommage dans la caractéristique en question lors de sa fuite.
* Si le jet est **un échec**, le personnage n'a pas pu fuir et doit donc continuer la confrontation.

## Dommages et récupération

### Infliger et encaisser des dégâts

Lorsqu'un personnage est blessé, il subit des dégâts dans la caractéristique liée. Ces points sont directement impactés dans sa réserve. On déduira toutefois la Protection induite par l'armure éventuelle.

> *Par exemple, Bob (Corps 7/7) est en train de combattre un scolopendre géant (Créature 8/8, Morsure 2) qui l'a attaqué pendant la nuit. L'effroyable bestiole l'attaque et réussit à le blesser grâce à un jet d'attaque (1d12 + 8 : 6+8 = 14, ce qui est supérieur à la défense passive de Bob : 12). Sa marge de réussite est de 2 et les dégâts de la morsure sont de 2, pour un total de 4 points de dommages. La réserve de Corps de Bob tombe donc à 3 points. Il vient d'être salement blessé.*

Lors d'une réussite critique, on calcule la marge de réussite + les dommages de l'arme et on multiplie par 2 le total.

### Récupération

Afin de récupérer sa réserve de caractéristique, plusieurs moyens sont envisageables.

* **Récupération naturelle :** Pour chaque tranche complète de 4h de repos, un personnage regagne un point de réserve. Cette récupération est doublée lorsque le personnage est en présence de sa source de *réconfort*.
* **Soin :** Il est possible de soigner des blessures physiques par un jet d'Esprit + Médecin. La difficulté du jet est égale à 12 + nombre de points à faire récupérer. Les blessures mentales peuvent être soignées par un jet d'Âme + Orateur de la même manière. Il n'est pas possible de soigner les blessures d'Âme de cette manière. Hélas, une réputation ne peut se reconstruire qu'avec le temps.
* **Fin de scénario :** À la fin d'un scénario, les personnages regagnent toutes leurs réserves jusqu'à leur valeur de caractéristique.

### Pertes définitives de caractéristique

Certaines blessures peuvent être particulièrement dévastatrices. Il est possible pour certaines armes et/ou créatures d'infliger des dommages irréversibles à un personnage. Lorsque cela arrive, le personnage ne perd pas de points de sa réserve mais directement dans sa valeur de caractéristique. Si la valeur devient inférieure à la réserve, réduisez alors la réserve pour qu'elle soit égale à la valeur.
