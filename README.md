# Don't Leave the Road - Document de référence système

Bienvenue sur le document de référence système de **Don't Leave the Road**!

Vous trouverez ici les règles du jeu dans leur dernière version, incluant les errata, téléchargements, et univers.

Ce dépôt inclut les fichiers source du site (sous Jekyll) au format Markdown.

Les règles et les univers ici présentés sont sous licence Creative Commons BY-NC-SA 4.0 International.
