---
layout: page
title: 03 - Création de personnage
toc: true
---

Il est assez rapide de créer un personnage pour *Don't Leave the Road*. Pour ce faire, munissez-vous d'une feuille de personnage, et de vos outils d'écriture favoris (de préférence, effaçables et réinscriptibles à loisir).

Vous pouvez trouver la feuille de personnage dans la section Téléchargements.

## 1. Choisir votre concept de personnage

En tout premier lieu, il est important de savoir quoi jouer. Par conséquent, la toute première chose à décider est le concept du personnage. Ce qu'il est, ce qu'il aime.

> *Bob a envie de jouer une combattante farouche qui s'énerve facilement. Il inscrit donc comme concept de personnage:* Épéiste au sale caractère.

Un bon concept devrait de préférence donner des indications sur la spécialité et un aspect de la psychologie du personnage.

Inscrivez également le nom du personnage si vous avez déjà choisi. Autrement, vous avez encore du temps pour décider durant les prochaines étapes.

## 2. Répartir les points de caractéristiques

Un personnage dispose de trois caractéristiques : Le **Corps**, l'**Esprit** et l'**Âme**. Chacune de ces caractéristiques peut avoir une valeur comprise entre 2 et 8 à la création. On considèrera que 5 est une valeur moyenne pour un humain lambda.

Un personnage débutant dispose de **17 points** à répartir entre ces trois caractéristiques.

> *Pour sa combattante, Bob a choisi de faire un personnage équilibré, mais dont la caractéristique d'Âme est assez basse, rapport à son caractère de cochon. Il répartit donc 7 points en Corps, 6 en Esprit et 4 en Âme.*

Un personnage débute avec **2 points de Destin**.

## 3. La Voie des Origines

La **Voie des Origines** permet de définir les *traits* (ou compétences) du personnage de manière cohérente avec l'univers et de créer un début de background.

### Grandir dans une communauté

Lorsqu'un bambin évolue dans une communauté, il apprend naturellement au contact des autres. C'est pourquoi le premier trait à **+1** est déterminé par l'activité principale de la communauté où il vit. Par exemple, un enfant ayant grandi dans un petit village vivant majoritairement de l'agriculture aura comme premier trait Paysan, tandis que s'il grandit dans un bourg spécialisé dans la briquèterie, il aura Artisan en premier trait.

Si vos joueurs veulent être originaires de la même communauté, ils auront donc ce trait en commun.

> *Le groupe de joueurs a décidé de créer des personnages originaires de la Colline-aux-Chênes, une communauté dont la spécialité est la charbonnerie. Leur trait d'origine sera donc Forestier. Les joueurs inscrivent donc le trait Forestier avec une valeur de +1.*

### Quand je serai grand, je ferai comme Papa !

On apprend toujours plus de ses parents, aussi le métier de l'un des parents apparaîtra en second trait à **+1**. On fera toutefois attention à sélectionner un trait différent du premier. Ce n'est pas forcément le métier du père, mais peut aussi être celui du grand-père, du tonton taré, ou même d'un mentor de l'enfance.

> *Bob a décidé que le père de son personnage était membre du conseil du village, et que son personnage a beaucoup appris de celui-ci sur la manière de gérer efficacement une communauté. Il inscrit donc le trait Administrateur avec la valeur de +1.*

### L'apprentissage final

Pour finir, le personnage a appris un métier à part, qui fait de lui quelqu'un de spécial dans sa communauté (et le fera sans doute partir sur les routes, le pauvre). Ce trait final a une valeur de **+2**, et doit être un trait différent des deux premiers.

> *Bob estime que son personnage a appris le métier des armes et inscrit donc le trait Soldat avec une valeur de +2. Son personnage a donc comme traits Forestier +1, Administrateur +1 et Soldat +2.*

### Option : Jouer un personnage expérimenté

Avec l'accord du MJ, il est possible de créer un personnage ayant un peu plus de bouteille. Dans ce cas, le joueur sélectionne un trait supplémentaire à +1, ou ajoute 1 à la valeur d'un de ses traits existants. Le personnage commencera avec un point de destin en moins, car l'expérience s'acquiert au prix de mille périls.

## 4. Les Aspects du personnage

Après avoir défini le **Concept** qui est le premier aspect, il reste encore à définir la **Crainte** et le **Réconfort**.

### La Crainte

Choisissez une crainte. C'est la principale peur de votre personnage. Soyez cohérent avec l'univers dans lequel vous vivez. La source de cette peur doit exister dans l'univers, et le MJ doit pouvoir l'utiliser à votre encontre.

> *Bob décide que son Épéiste se sent mal à l'aise lorsqu'elle doit nager sans voir le fond du plan d'eau où elle évolue. Il inscrit donc* Bathophobie *dans la Crainte de son personnage*.

### Le Réconfort

À présent, choisissez une source de réconfort. Cela peut être une personne, un objet, un lieu, en gros quelque chose de physique. Attention toutefois, l'avantage donné par la source de réconfort ne fonctionne que lorsque vous êtes physiquement en sa présence.

> *Malgré son sale caractère, l'Épéiste de Bob tient plus que tout à son jeune frère, qui est en passe d'accéder à l'âge d'Homme. Bob inscrit donc* Rudolf, mon frère de 14 ans *dans le Réconfort de son personnage. Il ne bénéficiera des avantages de ce Réconfort qu'en sa présence, au sein de son village.*

## 5. L'équipement

Décidez maintenant de l'équipement que portera votre personnage. Porte-t-il une armure lourde ? légère ? Avec quel type d'arme combat-il ? Soyez à nouveau cohérent avec l'univers. Pas de lance plasma dans un univers médiéval !

Discutez de cela avec votre MJ, qui sera ravi de vous accorder ou non les objets désirés. N'oubliez pas que la corruption de MJ est toujours possible, seul change le prix.

> *Bob et son MJ s'entendent sur le fait que son Épéiste doit posséder une épée - logique - ainsi qu'une armure intermédiaire - une cotte de maille léguée par son grand-père. Bob obtient de surcroît une besace fort pratique pour transporter des rations ainsi qu'une outre.*
