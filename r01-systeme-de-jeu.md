---
layout: page
title: 01 - Système de jeu
toc: true
---

Le système de jeu se veut très simple et axé sur la narration plus que sur des jets de dés à répétition. Le d12 est le principal (seul) dé utilisé.

## Caractéristiques

Les caractéristiques sont au nombre de 3 :

- **Corps :** Pour toutes les actions physiques et la résistance aux chocs.
- **Esprit :** Pour les actions mentales et la résistance à la folie et à la peur.
- **Âme :** Pour les actions sociales et la résistance aux intimidations et mensonges divers et variés.

La valeur d'une caractéristique se situe entre 0 (amorphe ou mort) et 12 (surhumanité totale). La moyenne humaine d'une caractéristique est de 5.


### Valeur et réserve de caractéristiques

La valeur de caractéristique est (pour un nouveau personnage) le nombre de points attribués à la caractéristique.

La réserve de caractéristique est le nombre actuel de points qu'a le personnage dans sa caractéristique. Elle monte et descend lors de l'histoire.

Lorsque la réserve de caractéristique arrive à 0, le personnage n'est plus jouable. Le joueur doit alors créer un nouveau personnage. À 0 de Corps, un personnage est mort, rekt, fini quoi. À 0 d'Esprit, il est complètement fou. À 0 d'Âme, il est catatonique et ne s'exprime plus que par grognements.

### Le corps

La caractéristique de Corps représente son aptitude physique. Un personnage fort, agile ou résistant sera donc doté d'un Corps élevé. Par opposition, une personne à la santé fragile ou maladroite aura un Corps faible. La réserve de Corps est également la santé physique, qui diminue lorsque le personnage se fait blesser physiquement.

<table>
<thead>
<tr>
<th>Valeur de corps</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr><td>12</td><td>Surhumain. Il n'est pas difficile de défoncer des murs de forteresse ou de courir sur l'eau.</td></tr>
<tr><td>10</td><td>Extrêmement fort. Vous êtes invaincu au bras de fer depuis des années et les microbes fuient en vous voyant.</td></tr>
<tr><td>8 </td><td>Très fort, agile et endurant. Maximum à la création.</td></tr>
<tr><td>5 </td><td>Moyenne humaine</td></tr>
<tr><td>3 </td><td>Faiblard. Minimum à la création</td></tr>
</tbody>
</table>

### L'Esprit

La caractéristique d'Esprit représente son aptitude mentale. Un personnage intelligent, astucieux ou attentif sera donc doté d'un Esprit élevé. Par opposition, une personne à la intelligence limitée ou peu réceptif aura un Esprit faible. La réserve d'Esprit est également la santé mentale, qui diminue lorsque le personnage est confronté à des événements traumatisants, ou se fait attaquer psychiquement.

<table>
<thead>
<tr>
<th>Valeur d'esprit</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr><td>12</td><td>Surhumain. Votre esprit surentraîné détecte les moindres incohérences, et votre perception est meilleure que celle du plus terrible prédateur.</td></tr>
<tr><td>10</td><td>Extrêmement doué. Vous pouvez résoudre des problèmes logiques en un instant et vous avez un oeil d'aigle.</td></tr>
<tr><td>8 </td><td>Très intelligent et perceptif. Maximum à la création.</td></tr>
<tr><td>5 </td><td>Moyenne humaine</td></tr>
<tr><td>3 </td><td>Myope comme une taupe ou stupide. Minimum à la création</td></tr>
</tbody>
</table>

### L'Âme

La caractéristique d'Âme représente son aptitude sociale. Un personnage charismatique ou beau sera donc doté d'une Âme élevée. Par opposition, une personne laids ou effacée aura une Âme faible. La réserve d'Âme est également la santé sociale, qui diminue lorsque le personnage se fait blesser socialement, comme lors d'un duel d'éloquence.

<table>
<thead>
<tr>
<th>Valeur d'âme</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr><td>12</td><td>Surhumain. Votre verve et votre beauté sont révérées dans les légendes.</td></tr>
<tr><td>10</td><td>Extrêmement charismatique. Il suffit que vous entriez dans une auberge pour que tous les regards se tournent vers vous.</td></tr>
<tr><td>8 </td><td>Très beau, charismatique et avec une belle verve. Maximum à la création.</td></tr>
<tr><td>5 </td><td>Moyenne humaine</td></tr>
<tr><td>3 </td><td>Laid ou effacé. Minimum à la création</td></tr>
</tbody>
</table>

### Prendre des dommages dans une caractéristique

Lorsqu'un personnage subit des dégâts physiques, ceux-ci sont directement impactés dans sa caractéristique de Corps.

Lorsqu'il subit une grande peur ou des émotions mentales divergentes (par exemple, en étant attiré et révulsé à la fois par la bête qu'il voit), cela crée des dégâts mentaux qui sont impactés dans la caractéristique d'Esprit

Lorsqu'il est intimidé fortement ou encore ridiculisé en public, c'est la caractéristique d'Âme qui est touchée.

### Regagner sa réserve de caractéristique

Lorsqu'un personnage se repose dans un lieu (relativement) sûr pendant quatre heures, il regagne un point dans une de ses réserves de caractéristiques. S'il choisit de se reposer huit heures, il aura donc deux points à répartir entre deux caractéristiques, ou deux points dans une seule, à choix. Trois points pour douze heures, etc... On parle ici de repos, cela peut donc être trainer dans le bar d'une communauté (si le bar est assez sûr, je déconseille à tous le bouge des trafiquants de cerveaux), dormir, ou encore entretenir son équipement.

À la fin d'un **Scénario**, un personnage remonte toutes ses réserves à la valeur de caractéristique.

## Aspects

Un personnage est défini par trois aspects :

### Concept

Le concept du personnage définit qui il est, quelle est sa spécialité, et quel peut être son trait de caractère le plus marquant. On définira par exemple un personnage comme _Bûcheron acariâtre_ ou encore _Bourgmestre cupide_.

### Réconfort

Il s'agit ici de quelque chose ou quelqu'un qui apporte un grand réconfort au personnage. Cela peut être une petite amie, un colifichet, une arme, tout (ou presque) est possible. Le réconfort permet au personnage de regagner ses points de caractéristique perdus deux fois plus vite, uniquement lorsqu'il est à proximité.

Dans le cas où le personnage a perdu (définitivement ou non) sa source de réconfort, il perd cette aptitude jusqu'à la fin d'un __Cycle__ (ou jusqu'à ce qu'il le retrouve, ce qui peut donner lieu à un Scénario ou à un Cycle complet). À la fin d'un Cycle, il peut choisir un nouveau Réconfort.

### Crainte

La crainte du personnage est sa plus grande peur. Elle doit être cohérente avec l'univers. Lorsqu'il est confronté à sa crainte, le personnage doit immédiatement faire un test d'Esprit à difficulté 12. S'il réussit, il a combattu sa peur et gagne un Point de Destin. S'il échoue, il perd un point d'Esprit.

## Points de Destin

Dans des cas extrêmes, un personnage peut influencer quelque peu le destin. Pour ce faire, il bénéficie de points de destin. Un point de destin peut être dépensé pour :

- Relancer un jet, quel que soit le résultat
- Éviter de mourir, devenir fou ou amorphe. Lorsque des dégâts devraient amener une de ses caractéristiques à 0, il peut dépenser un point de destin pour ramener sa réserve de caractéristique à 1, et être simplement inconscient. Le MJ ne devrait pas s'acharner sur le personnage, après tout, il vient de survivre à une mort certaine.

## Actions

<div class="message" style="text-align:center;">
  1d12 + Réserve de caractéristique + Valeur de trait (+/- Bonus/Malus)
</div>

Lorsque le résultat d'une action n'est pas automatiquement réussi ou automatiquement échoué, on lance 1d12 auquel on ajoute la réserve de caractéristique liée, et éventuellement un trait approprié, et on compare à un seuil de réussite.

- Si le résultat est supérieur au seuil, l'action est **réussie**.
- Si le résultat est égal, l'action est **réussie**, avec une **complication**
- Si le résultat est inférieur, l'action est **échouée**.

### Les extrêmes du dé

Si le résultat du dé est un 12 naturel, on dit que la réussite est critique. Cela entraîne les possibilités suivantes :

- Le personnage a si bien réussi son action, que le prochain jet en relation, que ce soit le sien ou celui de l'un de ses alliés, bénéficiera d'un bonus de +3.
- Le personnage a visé un point sensible de son adversaire, et inflige donc deux fois plus de dommages.
- L'action du personnage visait à handicaper son adversaire, qui subira donc un malus de -3 à son prochain jet en relation.

Si le résultat du dé est un 1 naturel, il s'agit d'un échec critique, et ce n'est généralement pas bon... Cela entraîne les possibilités suivantes :

- Non seulement le personnage a complètement raté son action, mais il s'est blessé dans la manoeuvre. Il perd un point de sa réserve de la caractéristique associée.
- En attaquant son adversaire, il a gravement ouvert sa garde. Lors de sa prochaine attaque, l'adversaire aura un bonus de +3.
- Quelque chose de très mauvais s'est produit. Le MJ peut imaginer n'importe quelle complication.

#### Option : Les confirmations de critiques

Si on désire que les critiques (échecs comme réussites) soient moins fréquents, il existe la possibilité de faire confirmer les critiques.Pour ce faire, lorsque le dé tombe sur un extrême, on relance le jet.

- Si le jet originel était un 12 naturel et que le résultat du second jet est supérieur ou égal à la difficulté, il s'agit d'une réussite critique. Sinon, il s'agira uniquement d'une réussite simple.
- Si le jet originel était un 1 naturel et que le résultat du second jet est inférieur à la difficulté, il s'agit d'un échec critique. Sinon, il s'agit d'un échec simple.

### Donner de sa personne

Lorsqu'un personnage effectue une action, il peut choisir de donner de soi-même pour réussir son action. (Cela peut être fait avant ou après l'annonce du résultat)

Lorsqu'il donne de sa personne, il dépense un point de la réserve de la caractéristique concernée, et peut alors faire augmenter le degré de réussite d'un cran. Attention, un échec critique restera un échec critique, et on ne peut pas obtenir de réussite critique de cette façon !

Dépenser un point de sa réserve peut donc :

- Faire passer d'un échec à une réussite avec complication
- Faire passer d'une réussite avec complication en réussite

## Les traits

Un personnage est défini par ses caractéristiques mais surtout par ses compétences appelées traits. Les traits sont décrits dans cette section. À la création, aucun trait ne peut dépasser la valeur de 3. La valeur d'un trait en général ne pourra jamais dépasser 5.

> Il existe certaines créatures et PNJ dont les traits dépassent 5, mais ces êtres sont rares au point de figurer dans les légendes. Penser *dragon*, *demi-dieu* ou encore *super-héros*.

Lors d'un jet de dés, on ne peut utiliser qu'un seul trait. Celui-ci est déterminé en concertation entre le MJ et le joueur.

<table>
<thead>
<tr>
<th>Valeur de trait</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr><td>5</td><td>Légendaire. On ne compte qu'une poignée d'îndividus aussi talentueux par génération.</td></tr>
<tr><td>4</td><td>Maître. Le personnage a une vie d'expérience dans le domaine, et est consulté régulièrement pour ses aptitudes.</td></tr>
<tr><td>3 </td><td>Expert. Le personnage est très doué dans ce domaine et est une sommité locale. Maximum à la création.</td></tr>
<tr><td>2 </td><td>Professionnel. Le personnage a suivi une formation et met en pratique ce trait régulièrement.</td></tr>
<tr><td>1 </td><td>Formé. Le personnage a suivi une formation dans le domaine</td></tr>
<tr><td>0 </td><td>Non formé. Il n'est pas forcément nécessaire d'être formé pour utiliser un trait, sauf en des cas raisonnablement logiques.<br /><em>Par exemple, il n'est pas possible de réparer une mécanique complexe sans avoir été entraîné au préalable.</em></td></tr>
</tbody>
</table>

